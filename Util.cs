﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace Prelab2
{
    public static class Util
    {
        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        /*reminder.csv ye yazma*/
        public static void WriteReminder(List<ClassReminder> reminder, string csvpath)
        {
            using (var writer = new StreamWriter(csvpath))
            {
                foreach (var reminders in reminder)
                {
                    writer.WriteLine(reminders.ToString());
                }
                writer.Close();
            }
        }

        /*reminder.csv den okuma*/
        public static void ReadReminder(List<ClassReminder> reminder, string csvpath)
        {
            var fileStream = new FileStream(csvpath, FileMode.OpenOrCreate, FileAccess.Read);
            using (var reader = new StreamReader(fileStream))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line != null)
                    {
                        var values = line.Split(';');
                        string instruction = values[0];
                        string date = values[1];
                        string time = values[2];
                        string type = values[3];
                        string username = values[4];
                        reminder.Add(new ClassReminder(instruction, date, time, type, username));
                    }
                }
                reader.Close();
            }
        }
    }   
}