﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace OOPLab
{
    public partial class FormLogin : Form
    {
        public static List<User> userList = new List<User>();/*kullanıcı listesi*/
        public FormLogin()
        {
            InitializeComponent();
            Util.ReadCsv(userList, @"data.csv");
        }
       // User admin = new User();
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (checkRemMe.Checked == true)
            {
                Properties.Settings.Default.username = txtUsername.Text;
                Properties.Settings.Default.password = txtPassword.Text;
                Properties.Settings.Default.Save();
            }
            if (checkRemMe.Checked == false)
            {
                Properties.Settings.Default.username = "";
                Properties.Settings.Default.password = "";
                Properties.Settings.Default.Save();
            }
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            for (int i = 0; i < userList.Count(); i++)
            {
                //admin= userList[0];
                User user = userList[i];
               
                if (user.IsValid(username, password))
                {
                    LoginedUser.getInstance().User = user;
                    lblLoginMess.Text = "SUCCESS";
                    lblLoginMess.ForeColor = Color.Green;
                    timreLogin.Interval = 3000;
                    timreLogin.Enabled = true;
                    return;
                }
                lblLoginMess.Text = "FAILURE";
                lblLoginMess.ForeColor = Color.Red;
                txtUsername.Clear();
                txtPassword.Clear();
            }
           
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormSignIn kayıt = new FormSignIn();
            kayıt.Show();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.username != string.Empty)
            {
                txtUsername.Text = Properties.Settings.Default.username;
                txtPassword.Text = Properties.Settings.Default.password;
                checkRemMe.Checked = true;
            }
        }

        private void timerLogin_Tick(object sender, EventArgs e)
        {
            this.Hide();
            PersonalWindow newuser = new PersonalWindow();
            newuser.Show();
            timreLogin.Enabled = false;
        }
    }
}
