﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Prelab2
{
    public partial class PersonalInformation : Form
    {
        public PersonalInformation()
        {
            InitializeComponent();
        }
        public event EventHandler<EventArgs> Undo;
        public event EventHandler<EventArgs> Redo;

        public int Capacity { get; set; }
        public int RedoesQuantity { get; set; }

        private void PersonalInformation_Load(object sender, EventArgs e)
        {
            txtName.Text = LoginedUser.getInstance().personalinformation.Name;
            txtSurname.Text = LoginedUser.getInstance().personalinformation.Surname;
            txtPhoneNumber.Text = LoginedUser.getInstance().personalinformation.PhoneNumber;
            txtAddress.Text = LoginedUser.getInstance().personalinformation.Address;
            txtEmail.Text = LoginedUser.getInstance().personalinformation.Email;
            txtPassword.Text = LoginForm.password;
            pictureProfil.Image = Image.FromFile(@"Resources\ProfilePictures\" + LoginForm.username+".jpg");
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines("Data.csv"); 
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Contains(";"))
                {
                    var split = line.Split(';');
                    if (split[0].Equals(LoginedUser.getInstance().User.Username))
                    {
                        split[0] = LoginedUser.getInstance().User.Username;
                        split[1] = Util.getHashSha256(txtPassword.Text);
                        split[2] = LoginedUser.getInstance().User.Usertype;
                        split[3] = txtName.Text;
                        split[4] = txtSurname.Text;
                        split[5] = txtPhoneNumber.Text;
                        split[6] = txtAddress.Text;
                        split[7] = txtEmail.Text;
                        line = string.Join(";", split);
                    }
                    lines[i] = line;
                }
            }
            File.WriteAllLines("Data.csv", lines);
        }

        Bitmap image;
        string base64Text;
        private void btnPhoto_Click(object sender, EventArgs e)
        {
            openFileDialog1.Multiselect = false;
            openFileDialog1.Filter = "All Files |*.png; *.jpeg;*.jpg| PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg";
            openFileDialog1.Title = "Select File";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                image = new Bitmap(openFileDialog1.FileName);
                pictureProfil.Image = (Image)image;

                byte[] imageArray = System.IO.File.ReadAllBytes(openFileDialog1.FileName);
                base64Text = Convert.ToBase64String(imageArray);

                string destPath = Application.StartupPath + @"\Resources\ProfilePictures\" + LoginForm.username + ".jpg";
                Directory.CreateDirectory(Application.StartupPath + @"\Resources\ProfilePictures\");
                File.Copy(openFileDialog1.FileName, destPath, true);
            }

            string path = @"photo.csv";
            using (StreamWriter stream = File.CreateText(path))
            {
                stream.Write(base64Text);
            }
            txtPhoto.Text = openFileDialog1.FileName.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (txtPassword.UseSystemPasswordChar == true)
                txtPassword.UseSystemPasswordChar = false;
            else
                txtPassword.UseSystemPasswordChar = true;
        }

        private void PersonalInformation_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show(this, "Really want to exit??", "Closing", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                    PersonalOrganizer personalOrganizer = new PersonalOrganizer();
                    personalOrganizer.Show();
                }
            }
        }
    }
}