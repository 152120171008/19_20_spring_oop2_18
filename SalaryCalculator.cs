﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Prelab2
{
    public partial class SalaryCalculator : Form
    {
        public SalaryCalculator()
        {
            InitializeComponent();
        }

        SalaryCalc salaryInfo;
        private void SalaryCalculator_Load(object sender, EventArgs e)
        {
            ReadCsv(@"salaryInfo.csv");
            for (int i = 0; i < User.salaryInfoList.Count(); i++)
            {
                LoginedUser.getInstance().SalaryCalc = User.salaryInfoList[i];
                if (LoginedUser.getInstance().SalaryCalc.Name == LoginedUser.getInstance().User.Username)
                {
                    cBoxExperience.Text = LoginedUser.getInstance().SalaryCalc.Experience.ToString();
                    cBoxProvince.Text = LoginedUser.getInstance().SalaryCalc.Province.ToString();
                    cBoxEducation.Text = LoginedUser.getInstance().SalaryCalc.Education.ToString();
                    cBoxForeignLanguage.Text = LoginedUser.getInstance().SalaryCalc.ForeignLanguage.ToString();
                    cBoxManagement.Text = LoginedUser.getInstance().SalaryCalc.ManagmentTask.ToString();
                    listBoxFamilyStatus.Text = LoginedUser.getInstance().SalaryCalc.FamilyStatus.ToString();
                }
            }
        }

        public static void WriteCsv(SalaryCalc salaryInfo, string csvpath)
        {
            FileStream file = new FileStream(csvpath, FileMode.Append);
            StreamWriter write = new StreamWriter(file);

            write.WriteLine(salaryInfo.ToString());
            write.Close();
            file.Close();
        }

        public static void ReadCsv(string csvpath)
        {
            var fileStream = new FileStream(csvpath, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    var values = line.Split(';');
                    string name = values[0];
                    string experience = values[1];
                    string province = values[2];
                    string education = values[3];
                    string foreignlanguage = values[4];
                    string managmenttask = values[5];
                    string familystatus = values[6];
                    User.salaryInfoList.Add(new SalaryCalc(name,experience, province,education, foreignlanguage, managmenttask, familystatus));  
                }
            }
            fileStream.Close();
        }
       
        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (cBoxExperience.Text == " " && cBoxProvince.Text == ""&& cBoxProvince.Text==""&& cBoxEducation.Text==""&& cBoxForeignLanguage.Text==""&& cBoxManagement.Text==""&&listBoxFamilyStatus.Text=="")
            {
                if (LoginedUser.getInstance().SalaryCalc.Name != LoginedUser.getInstance().User.Username)
                {
                    salaryInfo = new SalaryCalc(LoginedUser.getInstance().User.Username, cBoxExperience.SelectedItem.ToString(), cBoxProvince.SelectedItem.ToString(), cBoxEducation.SelectedItem.ToString(), cBoxForeignLanguage.SelectedItem.ToString(), cBoxManagement.SelectedItem.ToString(), listBoxFamilyStatus.SelectedItems.ToString());
                    WriteCsv(salaryInfo, @"salaryInfo.csv");
                }
                calculateSalary();
            }
            else
            {
                string[] lines = File.ReadAllLines("salaryInfo.csv"); // bilgileri güncelleme 
                for (int j = 0; j < lines.Length; j++)
                {
                    string line = lines[j];
                    if (line.Contains(";"))
                    {
                        var split = line.Split(';');
                        if (split[0].Equals(LoginedUser.getInstance().User.Username))
                        {
                            split[0] = LoginedUser.getInstance().User.Username;
                            split[1] = cBoxExperience.SelectedItem.ToString();
                            split[2] = cBoxProvince.SelectedItem.ToString();
                            split[3] = cBoxEducation.SelectedItem.ToString();
                            split[4] = cBoxForeignLanguage.SelectedItem.ToString();
                            split[5] = cBoxManagement.SelectedItem.ToString();
                            split[6] = listBoxFamilyStatus.SelectedItem.ToString();

                            line = string.Join(";", split);
                        }
                        lines[j] = line;
                    }
                }
                File.WriteAllLines("salaryInfo.csv", lines);

                calculateSalary();
            }
        }

        private void SalaryCalculator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show(this, "Really want to exit??", "Closing", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                    PersonalOrganizer personalOrganizer = new PersonalOrganizer();
                    personalOrganizer.Show();
                }
            }
        }

        private void calculateSalary()
        {
            int grossWages = 4500; //brüt ücret
            double coefficient = 0.00; //eklenecek katsayı

            // Experience
            if (cBoxExperience.Text == "0-2 years")
                coefficient += 0.00;
            else if (cBoxExperience.Text == "2-4 years")
                coefficient += 0.06;
            else if (cBoxExperience.Text == "5-9 years")
                coefficient += 1.00;
            else if (cBoxExperience.Text == "10-14 years")
                coefficient += 1.20;
            else if (cBoxExperience.Text == "15-20 years")
                coefficient += 1.35;
            else if (cBoxExperience.Text == "Over 20 years")
                coefficient += 1.50;

            // Province
            if (cBoxProvince.Text == "İstanbul")
                coefficient += 0.15;
            else if ((cBoxProvince.Text == "Ankara") || (cBoxProvince.Text == "İzmir"))
                coefficient += 0.10;
            else if ((cBoxProvince.Text == "Kocaeli,Sakarya,Düzce,Bolu,Yalova") || (cBoxProvince.Text == "Edirne,Kırklareli,Tekirdağ"))
                coefficient += 0.05;
            else if (cBoxProvince.Text == "Other")
                coefficient += 0.00;
            else
                coefficient += 0.03;

            // Education
            if (cBoxEducation.Text == "Vocational master's degree")
                coefficient += 0.10;
            else if (cBoxEducation.Text == "Doctorate about profession")
                coefficient += 0.30;
            else if (cBoxEducation.Text == "Professorship related to profession")
                coefficient += 0.35;
            else if (cBoxEducation.Text == "Non-professional master's degree")
                coefficient += 0.05;
            else if (cBoxEducation.Text == "PhD / associate professorship not related to the profession")
                coefficient += 0.15;

            // Foreign Language
            if ((cBoxForeignLanguage.Text == "Documented English knowledge") || (cBoxForeignLanguage.Text == "English language school graduation"))
                coefficient += 0.20;
            else if (cBoxForeignLanguage.Text == "Other documented foreign language knowledge")
                coefficient += 0.05;

            // Management Task
            if (cBoxManagement.Text == "No")
                coefficient += 0.00;
            else if (cBoxManagement.Text == "TeamLeader/GroupManager/TechnicalManager/Software/Architect")
                coefficient += 0.50;
            else if (cBoxManagement.Text == "Project Manager")
                coefficient += 0.75;
            else if (cBoxManagement.Text == "Director/ProjectsManager")
                coefficient += 0.85;
            else if (cBoxManagement.Text == "CTO/GeneralManager")
                coefficient += 1.00;
            else if (cBoxManagement.Text == "IT Manager/Manager(-5)")
                coefficient += 0.40;
            else if (cBoxManagement.Text == "IT Manager/Manager(+5)")
                coefficient += 0.60;

            // Family Status
            if (listBoxFamilyStatus.Text == "Not married")
                coefficient += 0.00;
            else if ((listBoxFamilyStatus.Text == "Married and spouse not working") || (listBoxFamilyStatus.Text == "0-6 year old child"))
                coefficient += 0.20;
            else if (listBoxFamilyStatus.Text == "7-18 year old child")
                coefficient += 0.30;
            else if (listBoxFamilyStatus.Text == "Child over 18")
                coefficient += 0.40;

            // Calculate
            lblSalary.Text = (grossWages * (coefficient + 1)).ToString();
        }  
    }
}