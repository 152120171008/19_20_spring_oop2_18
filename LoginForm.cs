﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.IO;

namespace Prelab2
{
    public partial class LoginForm : Form
    {
        public static List<User> userlist = new List<User>();
        public LoginForm()
        {
            InitializeComponent();
        }

        public static string username;
        public static string password;
       
        private void LoginBtn_Click(object sender, EventArgs e)
        {
            username = usernameBox.Text;
            password = passwordBox.Text;

            var fileStream = new FileStream("Data.csv", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    var values = line.Split(';');
                    string username = values[0];
                    string password = values[1];
                    string usertype = values[2];
                    string name = values[3];
                    string surname = values[4];
                    string phonenumber = values[5];
                    string address = values[6];
                    string email = values[7];
                    User.personalInfoList.Add(new personalinformation(name, surname, phonenumber, address, email));
                    userlist.Add(new User(username, password, usertype));
                }
            }
            fileStream.Close();

            infolabel.Visible = true;

            if (remembercheckBox.Checked)
            {
                Properties.Settings.Default["username"] = usernameBox.Text;
                Properties.Settings.Default["sifre"] = passwordBox.Text;
            }
            Properties.Settings.Default.Save();
           
            for (int i = 0; i < userlist.Count(); i++)
            {
                User user_data = userlist[i];
                if (user_data.IsValid(username, password))
                {
                    LoginedUser.getInstance().User = user_data;
                    LoginedUser.getInstance().personalinformation = User.personalInfoList[i];
                    infolabel.ForeColor = Color.Green;
                    infolabel.Text = "Correct.";
                    remembercheckBox.Checked = false;
                    EntryTime.Enabled = true;
                    return;
                }
                
                infolabel.ForeColor = Color.Red;
                infolabel.Text = "Not Correct.If you are not a member, please register first.";
                Properties.Settings.Default["username"] = "";
                Properties.Settings.Default["sifre"] = "";
                Properties.Settings.Default.Save();
            }
            usernameBox.Clear();
            passwordBox.Clear();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            infolabel.Visible = false;
            EntryTime.Enabled = false;
            usernameBox.Text = Properties.Settings.Default["username"].ToString();
            passwordBox.Text = Properties.Settings.Default["sifre"].ToString();
            if (usernameBox.Text.Count() > 1)
                remembercheckBox.Checked = true;
        }

        private void EntryTime_Tick(object sender, EventArgs e)
        {
            //this.Hide();
            PersonalOrganizer newuser = new PersonalOrganizer();
            newuser.Show();
            EntryTime.Enabled = false;
        }

        private void SignUpBtn_Click(object sender, EventArgs e)
        {
            SignUpForm frm = new SignUpForm();
            frm.Show();
        }

        private void usernameBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
                MessageBox.Show("Please enter only alphabetic characters.");
            }    
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show(this, "Really want to exit??", "Closing", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                }
            }
        }
    }
}