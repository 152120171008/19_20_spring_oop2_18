﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Prelab2
{
    public partial class Notes : Form
    {
        DataTable table = new DataTable();
        public Notes()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            table.Rows.Add(txtTitle.Text, txtMessage.Text);
            User.notes.Add(txtTitle.Text + ";" + txtMessage.Text.Replace(Environment.NewLine, "\\\\n") + ";" + LoginedUser.getInstance().User.Username);
            saveCsv();
            txtTitle.Clear();
            txtMessage.Clear();            
        }

        private void saveCsv()
        {
            FileStream file = new FileStream("notes.csv", FileMode.Append);
            using (StreamWriter writer = new StreamWriter(file))
            {
                foreach (var info in User.notes)
                {
                    writer.WriteLine(info.ToString());
                }
            }
            file.Close();
        }
        
        private void Notes_Load(object sender, EventArgs e)
        {
            table = new DataTable();
            table.Columns.Add("Title", typeof(string));
            table.Columns.Add("Message", typeof(string));
            gridViewList.DataSource = table;
            gridViewList.Columns["Message"].Visible = false;
            gridViewList.Columns["Title"].Width = 200;
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            table.Rows.Clear();
            string[] lines = File.ReadAllLines("notes.csv");
            string[] values;
            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].ToString().Split(';');

                if (values[2].Trim() == LoginedUser.getInstance().User.Username) 
                {
                    string[] row = new string[values.Length - 1];
                    for (int j = 0; j < values.Length - 1; j++)
                    {
                        if (j == 1)
                            values[j] = values[j].Replace("\\\\n", Environment.NewLine);

                        row[j] = values[j].Trim();
                    }
                    table.Rows.Add(row);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            UpdateUserList();
            string temp = gridViewList.CurrentRow.Cells["Title"].Value.ToString();
            int index;

            index = User.notes.BinarySearch(temp);
            User.notes.RemoveAt(index);
            //User.notes.Remove(temp + 1);
            //User.notes.Remove(temp + 2);


            saveCsv();
            txtTitle.Clear();
            txtMessage.Clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DataGridViewRow newDataRow = gridViewList.Rows[indexRow];
            string[] lines = File.ReadAllLines("notes.csv");
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Contains(";"))
                {
                    var split = line.Split(';');
                    if (split[0].Equals(newDataRow.Cells[0].Value.ToString()) && split[1].Equals(newDataRow.Cells[1].Value.ToString()))
                    {
                        split[1] = txtMessage.Text;
                        line = string.Join(";", split);
                    }
                    lines[i] = line;
                }
            }
            File.WriteAllLines("notes.csv", lines);
            UpdateUserList();
        }

        int indexRow;
        private void gridViewList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex; 
            DataGridViewRow row = gridViewList.Rows[indexRow];

            txtTitle.Text = row.Cells[0].Value.ToString();
            txtMessage.Text = row.Cells[1].Value.ToString();
        }

        private void UpdateUserList()
        {
            User.notes = new List<string>();
            var fileStream = new FileStream("notes.csv", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    var val = line.Split(';');
                    string title = val[0];
                    string notes = val[1];
                    string usertype = val[2];
                    User.notes.Add(title+";"+notes+";"+usertype);
                }
            }
            fileStream.Close();
        }

        private void gridViewList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Notes_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show(this, "Really want to exit??", "Closing", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                    PersonalOrganizer personalOrganizer = new PersonalOrganizer();
                    personalOrganizer.Show();
                }
            }
        }
    }
}