﻿namespace Prelab2
{
    partial class SalaryCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblYourSalary = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.listBoxFamilyStatus = new System.Windows.Forms.ListBox();
            this.cBoxManagement = new System.Windows.Forms.ComboBox();
            this.cBoxForeignLanguage = new System.Windows.Forms.ComboBox();
            this.cBoxEducation = new System.Windows.Forms.ComboBox();
            this.cBoxProvince = new System.Windows.Forms.ComboBox();
            this.cBoxExperience = new System.Windows.Forms.ComboBox();
            this.lblMy = new System.Windows.Forms.Label();
            this.lblFamilyStatus = new System.Windows.Forms.Label();
            this.lblManagement = new System.Windows.Forms.Label();
            this.lblForeignLanguage = new System.Windows.Forms.Label();
            this.lblEducation = new System.Windows.Forms.Label();
            this.lblProvince = new System.Windows.Forms.Label();
            this.lblExperience = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblYourSalary
            // 
            this.lblYourSalary.AutoSize = true;
            this.lblYourSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblYourSalary.Location = new System.Drawing.Point(512, 516);
            this.lblYourSalary.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYourSalary.Name = "lblYourSalary";
            this.lblYourSalary.Size = new System.Drawing.Size(155, 25);
            this.lblYourSalary.TabIndex = 71;
            this.lblYourSalary.Text = "Your Salary Is:";
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblSalary.Location = new System.Drawing.Point(693, 516);
            this.lblSalary.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(20, 25);
            this.lblSalary.TabIndex = 70;
            this.lblSalary.Text = "-";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(360, 512);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(100, 35);
            this.btnCalculate.TabIndex = 69;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            // 
            // listBoxFamilyStatus
            // 
            this.listBoxFamilyStatus.FormattingEnabled = true;
            this.listBoxFamilyStatus.ItemHeight = 20;
            this.listBoxFamilyStatus.Items.AddRange(new object[] {
            "Not married",
            "Married and spouse not working",
            "0-6 year old child",
            "7-18 year old child",
            "Child over 18"});
            this.listBoxFamilyStatus.Location = new System.Drawing.Point(247, 349);
            this.listBoxFamilyStatus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listBoxFamilyStatus.Name = "listBoxFamilyStatus";
            this.listBoxFamilyStatus.Size = new System.Drawing.Size(212, 104);
            this.listBoxFamilyStatus.TabIndex = 68;
            // 
            // cBoxManagement
            // 
            this.cBoxManagement.FormattingEnabled = true;
            this.cBoxManagement.Items.AddRange(new object[] {
            "No",
            "TeamLeader/GroupManager/TechnicalManager/Software/Architect",
            "Project Manager",
            "Director/ProjectsManager",
            "CTO/GeneralManager",
            "IT Manager/Manager(-5)",
            "IT Manager/Manager(+5)"});
            this.cBoxManagement.Location = new System.Drawing.Point(245, 295);
            this.cBoxManagement.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cBoxManagement.Name = "cBoxManagement";
            this.cBoxManagement.Size = new System.Drawing.Size(214, 28);
            this.cBoxManagement.TabIndex = 67;
            // 
            // cBoxForeignLanguage
            // 
            this.cBoxForeignLanguage.FormattingEnabled = true;
            this.cBoxForeignLanguage.Items.AddRange(new object[] {
            "Documented English knowledge",
            "English language school graduation",
            "Other documented foreign language knowledge"});
            this.cBoxForeignLanguage.Location = new System.Drawing.Point(245, 246);
            this.cBoxForeignLanguage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cBoxForeignLanguage.Name = "cBoxForeignLanguage";
            this.cBoxForeignLanguage.Size = new System.Drawing.Size(214, 28);
            this.cBoxForeignLanguage.TabIndex = 66;
            // 
            // cBoxEducation
            // 
            this.cBoxEducation.FormattingEnabled = true;
            this.cBoxEducation.Items.AddRange(new object[] {
            "Vocational master\'s degree",
            "Doctorate about profession",
            "Professorship related to profession",
            "Non-professional master\'s degree",
            "PhD / associate professorship not related to the profession"});
            this.cBoxEducation.Location = new System.Drawing.Point(245, 196);
            this.cBoxEducation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cBoxEducation.Name = "cBoxEducation";
            this.cBoxEducation.Size = new System.Drawing.Size(214, 28);
            this.cBoxEducation.TabIndex = 65;
            // 
            // cBoxProvince
            // 
            this.cBoxProvince.FormattingEnabled = true;
            this.cBoxProvince.Items.AddRange(new object[] {
            "İstanbul",
            "Ankara",
            "İzmir",
            "Kocaeli,Sakarya,Düzce,Bolu,Yalova",
            "Edirne,Kırklareli,Tekirdağ",
            "Trabzon,Ordu,Giresun,Rize,Artvin,Gümüşhane",
            "Bursa,Eskişehir,Bilecik",
            "Aydın,Denizli,Muğla",
            "Adana,Mersin",
            "Balıkesir,Çanakkale",
            "Antalya,Isparta,Burdur",
            "Other"});
            this.cBoxProvince.Location = new System.Drawing.Point(245, 153);
            this.cBoxProvince.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cBoxProvince.Name = "cBoxProvince";
            this.cBoxProvince.Size = new System.Drawing.Size(214, 28);
            this.cBoxProvince.TabIndex = 64;
            // 
            // cBoxExperience
            // 
            this.cBoxExperience.FormattingEnabled = true;
            this.cBoxExperience.Items.AddRange(new object[] {
            "0-2 years",
            "2-4 years",
            "5-9 years",
            "10-14 years",
            "15-20 years",
            "Over 20 years"});
            this.cBoxExperience.Location = new System.Drawing.Point(245, 106);
            this.cBoxExperience.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cBoxExperience.Name = "cBoxExperience";
            this.cBoxExperience.Size = new System.Drawing.Size(214, 28);
            this.cBoxExperience.TabIndex = 63;
            // 
            // lblMy
            // 
            this.lblMy.AutoSize = true;
            this.lblMy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMy.Location = new System.Drawing.Point(92, 56);
            this.lblMy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMy.Name = "lblMy";
            this.lblMy.Size = new System.Drawing.Size(170, 25);
            this.lblMy.TabIndex = 62;
            this.lblMy.Text = "Your Information";
            // 
            // lblFamilyStatus
            // 
            this.lblFamilyStatus.AutoSize = true;
            this.lblFamilyStatus.Location = new System.Drawing.Point(92, 349);
            this.lblFamilyStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFamilyStatus.Name = "lblFamilyStatus";
            this.lblFamilyStatus.Size = new System.Drawing.Size(109, 20);
            this.lblFamilyStatus.TabIndex = 61;
            this.lblFamilyStatus.Text = "Family Status:";
            // 
            // lblManagement
            // 
            this.lblManagement.AutoSize = true;
            this.lblManagement.Location = new System.Drawing.Point(92, 300);
            this.lblManagement.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblManagement.Name = "lblManagement";
            this.lblManagement.Size = new System.Drawing.Size(145, 20);
            this.lblManagement.TabIndex = 60;
            this.lblManagement.Text = "Management Task:";
            // 
            // lblForeignLanguage
            // 
            this.lblForeignLanguage.AutoSize = true;
            this.lblForeignLanguage.Location = new System.Drawing.Point(92, 250);
            this.lblForeignLanguage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForeignLanguage.Name = "lblForeignLanguage";
            this.lblForeignLanguage.Size = new System.Drawing.Size(143, 20);
            this.lblForeignLanguage.TabIndex = 59;
            this.lblForeignLanguage.Text = "Foreign Language:";
            // 
            // lblEducation
            // 
            this.lblEducation.AutoSize = true;
            this.lblEducation.Location = new System.Drawing.Point(92, 201);
            this.lblEducation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEducation.Name = "lblEducation";
            this.lblEducation.Size = new System.Drawing.Size(85, 20);
            this.lblEducation.TabIndex = 58;
            this.lblEducation.Text = "Education:";
            // 
            // lblProvince
            // 
            this.lblProvince.AutoSize = true;
            this.lblProvince.Location = new System.Drawing.Point(92, 160);
            this.lblProvince.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProvince.Name = "lblProvince";
            this.lblProvince.Size = new System.Drawing.Size(73, 20);
            this.lblProvince.TabIndex = 57;
            this.lblProvince.Text = "Province:";
            // 
            // lblExperience
            // 
            this.lblExperience.AutoSize = true;
            this.lblExperience.Location = new System.Drawing.Point(92, 110);
            this.lblExperience.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExperience.Name = "lblExperience";
            this.lblExperience.Size = new System.Drawing.Size(92, 20);
            this.lblExperience.TabIndex = 56;
            this.lblExperience.Text = "Experience:";
            // 
            // SalaryCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 638);
            this.Controls.Add(this.lblYourSalary);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.listBoxFamilyStatus);
            this.Controls.Add(this.cBoxManagement);
            this.Controls.Add(this.cBoxForeignLanguage);
            this.Controls.Add(this.cBoxEducation);
            this.Controls.Add(this.cBoxProvince);
            this.Controls.Add(this.cBoxExperience);
            this.Controls.Add(this.lblMy);
            this.Controls.Add(this.lblFamilyStatus);
            this.Controls.Add(this.lblManagement);
            this.Controls.Add(this.lblForeignLanguage);
            this.Controls.Add(this.lblEducation);
            this.Controls.Add(this.lblProvince);
            this.Controls.Add(this.lblExperience);
            this.Name = "SalaryCalculator";
            this.Text = "SalaryCalculator";
            this.Load += new System.EventHandler(this.SalaryCalculator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblYourSalary;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.ListBox listBoxFamilyStatus;
        private System.Windows.Forms.ComboBox cBoxManagement;
        private System.Windows.Forms.ComboBox cBoxForeignLanguage;
        private System.Windows.Forms.ComboBox cBoxEducation;
        private System.Windows.Forms.ComboBox cBoxProvince;
        private System.Windows.Forms.ComboBox cBoxExperience;
        private System.Windows.Forms.Label lblMy;
        private System.Windows.Forms.Label lblFamilyStatus;
        private System.Windows.Forms.Label lblManagement;
        private System.Windows.Forms.Label lblForeignLanguage;
        private System.Windows.Forms.Label lblEducation;
        private System.Windows.Forms.Label lblProvince;
        private System.Windows.Forms.Label lblExperience;
    }
}