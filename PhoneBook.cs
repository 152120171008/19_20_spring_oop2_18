﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Prelab2
{
    public partial class PhoneBook : Form
    {
        public PhoneBook()
        {
            InitializeComponent();
        }

        private void PhoneBook_Load(object sender, EventArgs e)
        {
            PhoneNumberDataGrd.Columns.Add("NAME", "NAME");
            PhoneNumberDataGrd.Columns.Add("SURNAME", "SURNAME");
            PhoneNumberDataGrd.Columns.Add("PHONE NUMBER", "PHONE NUMBER");
            PhoneNumberDataGrd.Columns.Add("ADDRESS", "ADDRESS");
            PhoneNumberDataGrd.Columns.Add("DESCRIPTION", "DESCRIPTION");
            PhoneNumberDataGrd.Columns.Add("E-MAIL", "E-MAIL");
        }

        private void CreateBttn_Click(object sender, EventArgs e)
        {
            PhoneNumberDataGrd.Rows.Add(NameBox.Text, SurnameBox.Text, PhoneNumBox.Text, AddressBox.Text, DescriptionBox.Text, MailBox.Text, LoginForm.username);
            NameBox.Text = "";
            SurnameBox.Text = "";
            PhoneNumBox.Text = "";
            AddressBox.Text = "";
            DescriptionBox.Text = "";
            MailBox.Text = "";
        }

        private void UpdateBttn_Click(object sender, EventArgs e)
        {
            DataGridViewRow newDataRow = PhoneNumberDataGrd.Rows[indexRow];
            newDataRow.Cells[0].Value = NameBox.Text;
            newDataRow.Cells[1].Value = SurnameBox.Text;
            newDataRow.Cells[2].Value = PhoneNumBox.Text;
            newDataRow.Cells[3].Value = AddressBox.Text;
            newDataRow.Cells[4].Value = DescriptionBox.Text;
            newDataRow.Cells[5].Value = MailBox.Text;
        }
        int indexRow;
        private void PhoneNumberDataGrd_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex; // get the selected Row Index
            DataGridViewRow row = PhoneNumberDataGrd.Rows[indexRow];

            NameBox.Text = row.Cells[0].Value.ToString();
            SurnameBox.Text = row.Cells[1].Value.ToString();
            PhoneNumBox.Text = row.Cells[2].Value.ToString();
            AddressBox.Text = row.Cells[3].Value.ToString();
            DescriptionBox.Text = row.Cells[4].Value.ToString();
            MailBox.Text = row.Cells[5].Value.ToString();
        }

        private void DeleteBttn_Click(object sender, EventArgs e)
        {
            if (PhoneNumberDataGrd.CurrentRow.Cells[0].Value != null)
            {
                PhoneNumberDataGrd.Rows.RemoveAt(PhoneNumberDataGrd.CurrentRow.Index);
            }
            else
            {
                MessageBox.Show("Please select the line to be deleted.");
            }
        }

        private void ListBttn_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines("phone.csv");
            string[] values;
            for (int i = 0; i < lines.Length; i++)
            {
                values = lines[i].ToString().Split(',');
                if (values[6].Trim() == LoginedUser.getInstance().User.Username) // giriş yapan kullanıcının phone bilgileri listelenmesi için
                {
                    string[] row = new string[values.Length];
                    for (int j = 0; j < values.Length; j++)
                    {
                        row[j] = values[j].Trim();
                    }
                    PhoneNumberDataGrd.Rows.Add(row);
                }
            }
        }

        private void PhoneNumBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
                MessageBox.Show("Please enter only numbers");
            }
        }

        private bool IsValidEmail(string strIn)
        {
            return Regex.IsMatch(strIn, "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");
        }

        private void MailBox_Leave(object sender, EventArgs e)
        {
            if (!IsValidEmail(MailBox.Text))
            {
                MessageBox.Show("Please enter a valid email.");
            }
        }

        private void SaveCsvBttn_Click(object sender, EventArgs e)
        {
            FileStream file = new FileStream("phone.csv", FileMode.Append);
            string sLine = "";
            for (int r = 0; r <= PhoneNumberDataGrd.Rows.Count - 1; r++)
            {
                for (int c = 0; c <= PhoneNumberDataGrd.Columns.Count - 1; c++)
                {
                    sLine = sLine + PhoneNumberDataGrd.Rows[r].Cells[c].Value;
                    if (c != PhoneNumberDataGrd.Columns.Count - 1)
                    {
                        sLine = sLine + ",";
                    }
                }
                User.phoneList.Add(sLine + "," + LoginedUser.getInstance().User.Username);
                sLine = "";
            }
            using (StreamWriter writer = new StreamWriter(file))
            {
                foreach (var info in User.phoneList)
                {
                    writer.WriteLine(info.ToString());
                }
            }
            file.Close();
        }

        private void PhoneBook_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show(this, "Really want to exit??", "Closing", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                    PersonalOrganizer personalOrganizer = new PersonalOrganizer();
                    personalOrganizer.Show();
                }
            }
        }
    }
}