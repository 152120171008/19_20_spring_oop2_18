﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Threading;

namespace Prelab2
{
    public partial class ManagementWindow : Form
    {
        public ManagementWindow()
        {
            InitializeComponent();
        }

        private void ManagementWindow_Load(object sender, EventArgs e)
        {

        }

        private void UserInfoList_SelectedIndexChanged(object sender, EventArgs e) { }

        int indexRow = 0;
        private void gridInfoList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indexRow = e.RowIndex;
            DataGridViewRow row = gridInfoList.Rows[indexRow];
            txtUpdate.Text = row.Cells[2].Value.ToString();
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            listeleGrid();
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            DataGridViewRow newDataRow = gridInfoList.Rows[indexRow];
            string[] lines = File.ReadAllLines("Data.csv");
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Contains(";"))
                {
                    var split = line.Split(';');
                    if (split[0].Equals(newDataRow.Cells[0].Value.ToString()) && split[1].Equals(newDataRow.Cells[1].Value.ToString()))
                    {
                        split[2] = txtUpdate.Text;
                        line = string.Join(";", split);
                    }
                    lines[i] = line;
                }
            }
            File.WriteAllLines("Data.csv", lines);
            listeleGrid();
        }

        private void listeleGrid()
        {
            LoginForm.userlist = new List<User>();

            var fileStream = new FileStream("Data.csv", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    var val = line.Split(';');
                    string username = val[0];
                    string password = val[1];
                    string usertype = val[2];
                    LoginForm.userlist.Add(new User(username, password, usertype));
                }
            }
            fileStream.Close();

            gridInfoList.Rows.Clear();
            string[] values;
            for (int i = 1; i < LoginForm.userlist.Count(); i++)
            {
                values = LoginForm.userlist[i].ToString().Split(';');
                string[] row = new string[values.Length];
                for (int j = 0; j < values.Length; j++)
                {
                    row[j] = values[j].Trim();
                }
                gridInfoList.Rows.Add(row);
            }
        }

        private string GenerationRandomPassword() // gönderilecek sifre icin rastgele karakterler olusturuyor
        {
            Random rastgele = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 8; i++)
            {
                int ascii = rastgele.Next(32, 127);
                char karakter = Convert.ToChar(ascii);
                sb.Append(karakter);
            }
            return (sb.ToString());
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            lblProcessing.Visible = true;
            progressBarEmail.Visible = true;
            if (!backgroundProcess.IsBusy)
                backgroundProcess.RunWorkerAsync();
        }

        private void backgroundProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            string newPassword = GenerationRandomPassword();
            SmtpClient sc = new SmtpClient();
            sc.Port = 587;
            sc.Host = "smtp.gmail.com";
            sc.EnableSsl = true;
            sc.Credentials = new NetworkCredential(LoginedUser.getInstance().User.Email, "apk12345"); 
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(LoginedUser.getInstance().User.Email);
            mail.To.Add(gridInfoList.Rows[indexRow].Cells[3].Value.ToString()); // sifre gönderilecek yani herhangi bir kullanıcının maili, gridden secilenin.
            mail.Subject = "Reset Email";
            mail.IsBodyHtml = true;
            mail.Body = newPassword;

            for (int i = 1; i <= 10; i++)
            {
                if (backgroundProcess.CancellationPending)
                {
                    e.Cancel = true;
                    backgroundProcess.ReportProgress(0);
                    return;
                }
                else
                {
                    Thread.Sleep(1000);
                    backgroundProcess.ReportProgress(i * 10);
                }
            }
            sc.Send(mail);
        }

        private void backgroundProcess_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarEmail.Value = e.ProgressPercentage;
            lblProcessing.Text = e.ProgressPercentage.ToString() + "%";
        }

        private void backgroundProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled) MessageBox.Show("Stopped.");
            else if (e.Error != null) MessageBox.Show(e.Error.Message);
            else MessageBox.Show("Your message has been sent.");
        }

        private void ManagementWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show(this, "Really want to exit??", "Closing", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = false;
                    PersonalOrganizer personalOrganizer = new PersonalOrganizer();
                    personalOrganizer.Show();
                }
            }
        }
    }
}