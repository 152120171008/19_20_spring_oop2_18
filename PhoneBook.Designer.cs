﻿namespace Prelab2
{
    partial class PhoneBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveCsvBttn = new System.Windows.Forms.Button();
            this.DeleteBttn = new System.Windows.Forms.Button();
            this.UpdateBttn = new System.Windows.Forms.Button();
            this.CreateBttn = new System.Windows.Forms.Button();
            this.ListBttn = new System.Windows.Forms.Button();
            this.MailLbl = new System.Windows.Forms.Label();
            this.MailBox = new System.Windows.Forms.TextBox();
            this.DescriptionLbl = new System.Windows.Forms.Label();
            this.PhoneNumberDataGrd = new System.Windows.Forms.DataGridView();
            this.AddressLbl = new System.Windows.Forms.Label();
            this.DescriptionBox = new System.Windows.Forms.TextBox();
            this.NumberLbl = new System.Windows.Forms.Label();
            this.SurnameLbl = new System.Windows.Forms.Label();
            this.AddressBox = new System.Windows.Forms.TextBox();
            this.NameLbl = new System.Windows.Forms.Label();
            this.PhoneNumBox = new System.Windows.Forms.TextBox();
            this.SurnameBox = new System.Windows.Forms.TextBox();
            this.NameBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PhoneNumberDataGrd)).BeginInit();
            this.SuspendLayout();
            // 
            // SaveCsvBttn
            // 
            this.SaveCsvBttn.Location = new System.Drawing.Point(830, 125);
            this.SaveCsvBttn.Name = "SaveCsvBttn";
            this.SaveCsvBttn.Size = new System.Drawing.Size(102, 38);
            this.SaveCsvBttn.TabIndex = 42;
            this.SaveCsvBttn.Text = "Save CSV";
            this.SaveCsvBttn.UseVisualStyleBackColor = true;
            // 
            // DeleteBttn
            // 
            this.DeleteBttn.Location = new System.Drawing.Point(481, 127);
            this.DeleteBttn.Name = "DeleteBttn";
            this.DeleteBttn.Size = new System.Drawing.Size(94, 36);
            this.DeleteBttn.TabIndex = 40;
            this.DeleteBttn.Text = "Delete";
            this.DeleteBttn.UseVisualStyleBackColor = true;
            // 
            // UpdateBttn
            // 
            this.UpdateBttn.Location = new System.Drawing.Point(282, 125);
            this.UpdateBttn.Name = "UpdateBttn";
            this.UpdateBttn.Size = new System.Drawing.Size(102, 38);
            this.UpdateBttn.TabIndex = 39;
            this.UpdateBttn.Text = "Update";
            this.UpdateBttn.UseVisualStyleBackColor = true;
            this.UpdateBttn.Click += new System.EventHandler(this.UpdateBttn_Click);
            // 
            // CreateBttn
            // 
            this.CreateBttn.Location = new System.Drawing.Point(91, 125);
            this.CreateBttn.Name = "CreateBttn";
            this.CreateBttn.Size = new System.Drawing.Size(87, 38);
            this.CreateBttn.TabIndex = 38;
            this.CreateBttn.Text = "Create";
            this.CreateBttn.UseVisualStyleBackColor = true;
            this.CreateBttn.Click += new System.EventHandler(this.CreateBttn_Click);
            // 
            // ListBttn
            // 
            this.ListBttn.Location = new System.Drawing.Point(659, 127);
            this.ListBttn.Name = "ListBttn";
            this.ListBttn.Size = new System.Drawing.Size(95, 38);
            this.ListBttn.TabIndex = 37;
            this.ListBttn.Text = "List";
            this.ListBttn.UseVisualStyleBackColor = true;
            // 
            // MailLbl
            // 
            this.MailLbl.AutoSize = true;
            this.MailLbl.Location = new System.Drawing.Point(617, 49);
            this.MailLbl.Name = "MailLbl";
            this.MailLbl.Size = new System.Drawing.Size(53, 20);
            this.MailLbl.TabIndex = 29;
            this.MailLbl.Text = "E-mail";
            // 
            // MailBox
            // 
            this.MailBox.Location = new System.Drawing.Point(734, 50);
            this.MailBox.Name = "MailBox";
            this.MailBox.Size = new System.Drawing.Size(100, 26);
            this.MailBox.TabIndex = 35;
            // 
            // DescriptionLbl
            // 
            this.DescriptionLbl.AutoSize = true;
            this.DescriptionLbl.Location = new System.Drawing.Point(617, 21);
            this.DescriptionLbl.Name = "DescriptionLbl";
            this.DescriptionLbl.Size = new System.Drawing.Size(89, 20);
            this.DescriptionLbl.TabIndex = 28;
            this.DescriptionLbl.Text = "Description";
            // 
            // PhoneNumberDataGrd
            // 
            this.PhoneNumberDataGrd.AllowUserToAddRows = false;
            this.PhoneNumberDataGrd.AllowUserToDeleteRows = false;
            this.PhoneNumberDataGrd.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PhoneNumberDataGrd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PhoneNumberDataGrd.Location = new System.Drawing.Point(16, 198);
            this.PhoneNumberDataGrd.Name = "PhoneNumberDataGrd";
            this.PhoneNumberDataGrd.ReadOnly = true;
            this.PhoneNumberDataGrd.RowTemplate.Height = 28;
            this.PhoneNumberDataGrd.Size = new System.Drawing.Size(867, 385);
            this.PhoneNumberDataGrd.TabIndex = 36;
            this.PhoneNumberDataGrd.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PhoneNumberDataGrd_CellClick);
            // 
            // AddressLbl
            // 
            this.AddressLbl.AutoSize = true;
            this.AddressLbl.Location = new System.Drawing.Point(339, 21);
            this.AddressLbl.Name = "AddressLbl";
            this.AddressLbl.Size = new System.Drawing.Size(68, 20);
            this.AddressLbl.TabIndex = 27;
            this.AddressLbl.Text = "Address";
            // 
            // DescriptionBox
            // 
            this.DescriptionBox.Location = new System.Drawing.Point(734, 18);
            this.DescriptionBox.Name = "DescriptionBox";
            this.DescriptionBox.Size = new System.Drawing.Size(100, 26);
            this.DescriptionBox.TabIndex = 34;
            // 
            // NumberLbl
            // 
            this.NumberLbl.AutoSize = true;
            this.NumberLbl.Location = new System.Drawing.Point(12, 80);
            this.NumberLbl.Name = "NumberLbl";
            this.NumberLbl.Size = new System.Drawing.Size(115, 20);
            this.NumberLbl.TabIndex = 26;
            this.NumberLbl.Text = "Phone Number";
            // 
            // SurnameLbl
            // 
            this.SurnameLbl.AutoSize = true;
            this.SurnameLbl.Location = new System.Drawing.Point(12, 49);
            this.SurnameLbl.Name = "SurnameLbl";
            this.SurnameLbl.Size = new System.Drawing.Size(74, 20);
            this.SurnameLbl.TabIndex = 25;
            this.SurnameLbl.Text = "Surname";
            // 
            // AddressBox
            // 
            this.AddressBox.Location = new System.Drawing.Point(425, 21);
            this.AddressBox.Multiline = true;
            this.AddressBox.Name = "AddressBox";
            this.AddressBox.Size = new System.Drawing.Size(150, 87);
            this.AddressBox.TabIndex = 33;
            // 
            // NameLbl
            // 
            this.NameLbl.AutoSize = true;
            this.NameLbl.Location = new System.Drawing.Point(12, 21);
            this.NameLbl.Name = "NameLbl";
            this.NameLbl.Size = new System.Drawing.Size(51, 20);
            this.NameLbl.TabIndex = 24;
            this.NameLbl.Text = "Name";
            // 
            // PhoneNumBox
            // 
            this.PhoneNumBox.Location = new System.Drawing.Point(176, 82);
            this.PhoneNumBox.Name = "PhoneNumBox";
            this.PhoneNumBox.Size = new System.Drawing.Size(100, 26);
            this.PhoneNumBox.TabIndex = 32;
            // 
            // SurnameBox
            // 
            this.SurnameBox.Location = new System.Drawing.Point(176, 50);
            this.SurnameBox.Name = "SurnameBox";
            this.SurnameBox.Size = new System.Drawing.Size(100, 26);
            this.SurnameBox.TabIndex = 31;
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(176, 18);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(100, 26);
            this.NameBox.TabIndex = 30;
            // 
            // PhoneBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 607);
            this.Controls.Add(this.SaveCsvBttn);
            this.Controls.Add(this.DeleteBttn);
            this.Controls.Add(this.UpdateBttn);
            this.Controls.Add(this.CreateBttn);
            this.Controls.Add(this.ListBttn);
            this.Controls.Add(this.MailLbl);
            this.Controls.Add(this.MailBox);
            this.Controls.Add(this.DescriptionLbl);
            this.Controls.Add(this.PhoneNumberDataGrd);
            this.Controls.Add(this.AddressLbl);
            this.Controls.Add(this.DescriptionBox);
            this.Controls.Add(this.NumberLbl);
            this.Controls.Add(this.SurnameLbl);
            this.Controls.Add(this.AddressBox);
            this.Controls.Add(this.NameLbl);
            this.Controls.Add(this.PhoneNumBox);
            this.Controls.Add(this.SurnameBox);
            this.Controls.Add(this.NameBox);
            this.Name = "PhoneBook";
            this.Text = "PhoneBook";
            this.Load += new System.EventHandler(this.PhoneBook_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PhoneNumberDataGrd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SaveCsvBttn;
        private System.Windows.Forms.Button DeleteBttn;
        private System.Windows.Forms.Button UpdateBttn;
        private System.Windows.Forms.Button CreateBttn;
        private System.Windows.Forms.Button ListBttn;
        private System.Windows.Forms.Label MailLbl;
        private System.Windows.Forms.TextBox MailBox;
        private System.Windows.Forms.Label DescriptionLbl;
        private System.Windows.Forms.DataGridView PhoneNumberDataGrd;
        private System.Windows.Forms.Label AddressLbl;
        private System.Windows.Forms.TextBox DescriptionBox;
        private System.Windows.Forms.Label NumberLbl;
        private System.Windows.Forms.Label SurnameLbl;
        private System.Windows.Forms.TextBox AddressBox;
        private System.Windows.Forms.Label NameLbl;
        private System.Windows.Forms.TextBox PhoneNumBox;
        private System.Windows.Forms.TextBox SurnameBox;
        private System.Windows.Forms.TextBox NameBox;
    }
}