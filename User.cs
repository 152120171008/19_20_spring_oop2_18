﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace Prelab2
{
    public class User
    {
        private String username;
        private String password;
        private String usertype;
        public static List<string> phoneList = new List<string>();
        public static List<string> notes = new List<string>();
        public static List<personalinformation> personalInfoList = new List<personalinformation>();
        public static List<SalaryCalc> salaryInfoList = new List<SalaryCalc>();
        public static List<ClassReminder> reminderList = new List<ClassReminder>();
        public User()
        {
        }
        public User(String username, String password,String usertype)
        {
            this.Username = username;
            this.Password = password;
            this.Usertype = usertype;           
        }

        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Usertype { get => usertype; set => usertype = value; }

        public bool IsValid(string username, string password)
        {
            return this.Username.Equals(username) && this.Password.Equals(Util.getHashSha256(password));
        }

        public string ToString()
        {
            return Username + ";" + Password + ";" + Usertype;
        }
    }
}