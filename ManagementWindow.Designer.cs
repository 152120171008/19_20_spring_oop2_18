﻿namespace Prelab2
{
    partial class ManagementWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Usertype = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Surname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridInfoList = new System.Windows.Forms.DataGridView();
            this.txtUpdate = new System.Windows.Forms.TextBox();
            this.btnList = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.lblProcessing = new System.Windows.Forms.Label();
            this.progressBarEmail = new System.Windows.Forms.ProgressBar();
            this.backgroundProcess = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gridInfoList)).BeginInit();
            this.SuspendLayout();
            // 
            // Usertype
            // 
            this.Usertype.HeaderText = "Usertype";
            this.Usertype.Name = "Usertype";
            // 
            // Password
            // 
            this.Password.HeaderText = "Password";
            this.Password.Name = "Password";
            // 
            // Surname
            // 
            this.Surname.HeaderText = "Surname";
            this.Surname.Name = "Surname";
            // 
            // gridInfoList
            // 
            this.gridInfoList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridInfoList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Surname,
            this.Password,
            this.Usertype});
            this.gridInfoList.Location = new System.Drawing.Point(30, 31);
            this.gridInfoList.Name = "gridInfoList";
            this.gridInfoList.RowTemplate.Height = 28;
            this.gridInfoList.Size = new System.Drawing.Size(385, 251);
            this.gridInfoList.TabIndex = 3;
            this.gridInfoList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridInfoList_CellClick);
            // 
            // txtUpdate
            // 
            this.txtUpdate.Location = new System.Drawing.Point(86, 366);
            this.txtUpdate.Name = "txtUpdate";
            this.txtUpdate.Size = new System.Drawing.Size(100, 26);
            this.txtUpdate.TabIndex = 4;
            // 
            // btnList
            // 
            this.btnList.Location = new System.Drawing.Point(293, 303);
            this.btnList.Name = "btnList";
            this.btnList.Size = new System.Drawing.Size(184, 57);
            this.btnList.TabIndex = 5;
            this.btnList.Text = "List";
            this.btnList.UseVisualStyleBackColor = true;
            this.btnList.Click += new System.EventHandler(this.btnList_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(86, 303);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(170, 57);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click_1);
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.Honeydew;
            this.btnSend.Location = new System.Drawing.Point(556, 314);
            this.btnSend.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(104, 35);
            this.btnSend.TabIndex = 12;
            this.btnSend.Text = "Send Email";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lblProcessing
            // 
            this.lblProcessing.AutoSize = true;
            this.lblProcessing.Location = new System.Drawing.Point(566, 373);
            this.lblProcessing.Name = "lblProcessing";
            this.lblProcessing.Size = new System.Drawing.Size(103, 20);
            this.lblProcessing.TabIndex = 15;
            this.lblProcessing.Text = "Processing....";
            // 
            // progressBarEmail
            // 
            this.progressBarEmail.BackColor = System.Drawing.Color.Honeydew;
            this.progressBarEmail.Location = new System.Drawing.Point(470, 396);
            this.progressBarEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBarEmail.Name = "progressBarEmail";
            this.progressBarEmail.Size = new System.Drawing.Size(266, 22);
            this.progressBarEmail.TabIndex = 14;
            // 
            // backgroundProcess
            // 
            this.backgroundProcess.WorkerReportsProgress = true;
            this.backgroundProcess.WorkerSupportsCancellation = true;
            this.backgroundProcess.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundProcess_DoWork);
            this.backgroundProcess.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundProcess_ProgressChanged);
            this.backgroundProcess.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundProcess_RunWorkerCompleted);
            // 
            // ManagementWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 449);
            this.Controls.Add(this.lblProcessing);
            this.Controls.Add(this.progressBarEmail);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnList);
            this.Controls.Add(this.txtUpdate);
            this.Controls.Add(this.gridInfoList);
            this.Name = "ManagementWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManagementWindow";
            this.Load += new System.EventHandler(this.ManagementWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridInfoList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn Usertype;
        private System.Windows.Forms.DataGridViewTextBoxColumn Password;
        private System.Windows.Forms.DataGridViewTextBoxColumn Surname;
        private System.Windows.Forms.DataGridView gridInfoList;
        private System.Windows.Forms.TextBox txtUpdate;
        private System.Windows.Forms.Button btnList;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label lblProcessing;
        private System.Windows.Forms.ProgressBar progressBarEmail;
        private System.ComponentModel.BackgroundWorker backgroundProcess;
    }
}